package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.enumerated.Status;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.dto.Task;

import java.util.Comparator;
import java.util.List;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    //List<Task> findAll(@NotNull String userId, Comparator<Task> comparator);

    @NotNull
    Task add(@NotNull String userId, @Nullable String name, @Nullable String description);

    void clear(@NotNull String userId);

    void clear();

    //void removeOneById(@NotNull String userId, @Nullable String id);

    //@Nullable
    //Task findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    Task findOneByName(@NotNull String userId, @Nullable String name);

    void removeOneByName(@NotNull String userId, @NotNull String name);

    //void removeTaskByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull Task finishTaskByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    Task findOneByIndex(@NotNull String userId, @Nullable Integer index);

    Task finishTaskById(@NotNull String userId, @Nullable String id);

    @Nullable
    Task finishTaskByName(@NotNull String userId, @Nullable String name);

    //@Nullable
    //Task changeTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    //@Nullable
    //Task changeTaskStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    //@Nullable
    //Task changeTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    //String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    //int size(@NotNull String userId);

    //boolean existsByName(@NotNull String userId, @NotNull String name);

    //boolean existsById(@NotNull String userId, @NotNull String id);

    //void setTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    //void setTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    //void setTaskStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

    @NotNull Task startTaskById(@NotNull String userId, @Nullable String id);

    @NotNull Task startTaskByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull Task startTaskByName(@NotNull String userId, @Nullable String name);

    @NotNull Task updateTaskById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull Task updateTaskByIndex(@NotNull String userId, @NotNull Integer index, @Nullable String name, @Nullable String description);

    //void updateTaskByName(@NotNull String userId, @Nullable String name, @Nullable String nameNew, @Nullable String description);

    @NotNull
    @SneakyThrows
    List<Task> findAll(@NotNull String userId);

    @SneakyThrows
    void addAll(@NotNull String userId, @Nullable Collection<Task> collection);

    @Nullable
    @SneakyThrows
    Task add(@NotNull String userId, @Nullable Task entity);

    @Nullable
    @SneakyThrows
    Task findById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void removeById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void remove(@NotNull String userId, @Nullable Task entity);

}
