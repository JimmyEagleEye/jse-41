package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.dto.Task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.util.List;

public interface IProjectTaskService {

    @Nullable
    List<Task> findALLTaskByProjectId(@NotNull String userId, @Nullable String projectId);

    @Nullable
    void assignTaskByProjectId(final String userId, final String taskId, final @Nullable String projectId);

    @Nullable
    void unassignTaskByProjectId(@NotNull String userId, @NotNull String taskId);

    void removeProjectById(@NotNull String userId, @NotNull String projectId);

    void removeProjectByName(@NotNull String userId, @Nullable String projectName);

    void removeProjectByIndex(@NotNull String userId, @NotNull Integer projectIndex);

    //void clearTasks(@NotNull String userId);

    //void setTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

}
