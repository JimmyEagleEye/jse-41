package ru.korkmasov.tsc.api.repository;

import ru.korkmasov.tsc.dto.AbstractEntity;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.dto.Session;

public interface IRepository<E extends AbstractEntity> {

    E add(final E entity);

    @Nullable
    E findById(@NotNull String id) throws SQLException;

    void addAll(final Collection<E> collection);

    void clear() throws SQLException;

    void remove(@Nullable E entity);

    void removeById(@NotNull String id) throws SQLException;

    //int size(@NotNull String testUserId);

    @NotNull
    List<E> findAll();

}
