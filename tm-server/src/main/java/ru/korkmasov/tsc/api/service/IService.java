package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.api.repository.IRepository;
import ru.korkmasov.tsc.dto.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
