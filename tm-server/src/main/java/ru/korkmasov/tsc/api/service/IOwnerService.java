package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.api.repository.IOwnerRepository;
import ru.korkmasov.tsc.dto.AbstractOwner;

public interface IOwnerService<E extends AbstractOwner> extends IService<E>, IOwnerRepository<E> {
}
