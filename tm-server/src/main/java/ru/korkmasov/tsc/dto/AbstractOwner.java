package ru.korkmasov.tsc.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.entity.ITWBS;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractOwner extends AbstractEntity {

    @Nullable
    @Column(name = "user_id")
    private String userId;

}


