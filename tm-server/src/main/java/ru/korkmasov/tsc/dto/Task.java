package ru.korkmasov.tsc.dto;

import ru.korkmasov.tsc.api.entity.ITWBS;
import ru.korkmasov.tsc.dto.AbstractOwner;
import ru.korkmasov.tsc.enumerated.Status;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@Entity
@Table(name = "tm_task")
@NoArgsConstructor
public class Task extends AbstractOwner implements ITWBS {

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @Nullable
    @Column(name = "start_date")
    private Date startDate;

    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;

    @Column
    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    public void setStatus(@Nullable final Status status) {
        if (status == null) return;
        this.status = status;
        switch (status) {
            case IN_PROGRESS:
                this.setDateStart(new Date());
                break;
            case COMPLETE:
                this.setDateFinish(new Date());
            default:
                break;
        }
    }

    @NotNull
    public Task(@Nullable String name) {
        this.name = name;
    }

    @NotNull
    public Task(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
