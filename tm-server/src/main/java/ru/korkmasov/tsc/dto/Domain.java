package ru.korkmasov.tsc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter

public class Domain implements Serializable {

    @JsonProperty("project")
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlElementWrapper(localName = "projects")
    @Nullable
    private List<Project> projects;

    @JsonProperty("task")
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlElementWrapper(localName = "tasks")
    @Nullable
    private List<Task> tasks;

    @JsonProperty("user")
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlElementWrapper(localName = "users")
    @Nullable
    private List<User> users;

    public void setUsers(@NotNull List<User> users) {
        this.users = users;
    }

    public void setProjects(@NotNull List<Project> projects) {
        this.projects = projects;
    }

    public void setTasks(@NotNull List<Task> tasks) {
        this.tasks = tasks;
    }
}
