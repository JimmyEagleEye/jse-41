package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.endpoint.Project;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.util.TerminalUtil;

import java.util.Arrays;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectByIdSetStatusCommand extends AbstractProjectCommand {


    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-change-status-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Change project status by id";
    }

    @Override
    public void execute() {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(serviceLocator.getSession(), id);
        System.out.println("ENTER STATUS:");
        @Nullable final String status = TerminalUtil.nextLine();
        System.out.println(Arrays.toString(Status.values()));
        //@Nullable final Project projectUpdated = serviceLocator.getProjectEndpoint().updateProjectById(serviceLocator.getSession(), id, status);
    }


}

