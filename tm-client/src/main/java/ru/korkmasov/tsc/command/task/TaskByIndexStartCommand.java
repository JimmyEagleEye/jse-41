package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.endpoint.Task;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskByIndexStartCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-start-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Start task by index";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Task task = serviceLocator.getTaskEndpoint().startTaskByIndex(serviceLocator.getSession(), index);
        if (task == null) throw new TaskNotFoundException();
    }

}
