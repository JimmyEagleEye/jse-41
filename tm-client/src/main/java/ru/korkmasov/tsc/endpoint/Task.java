package ru.korkmasov.tsc.endpoint;

import ru.korkmasov.tsc.enumerated.Status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "task", propOrder = {
        "name",
        "description",
        "status",
        "projectId",
        "startDate",
        "finishDate",
        "created"
})
public class Task
        extends AbstractBusinessEntity {

    protected String name;
    protected String description;
    @XmlSchemaType(name = "string")
    protected Status status;
    protected String projectId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar finishDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar created;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status value) {
        this.status = value;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String value) {
        this.projectId = value;
    }

    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    public XMLGregorianCalendar getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(XMLGregorianCalendar value) {
        this.finishDate = value;
    }

    public XMLGregorianCalendar getCreated() {
        return created;
    }

    public void setCreated(XMLGregorianCalendar value) {
        this.created = value;
    }

}
