
package ru.korkmasov.tsc.endpoint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findAllByUserIdResponse", propOrder = {
    "_return"
})
public class FindAllByUserIdResponse {

    @XmlElement(name = "return")
    protected List<Session> _return;

    /**
     * Gets the value of the return property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the return property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getReturn().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link Session }
     * 
     * 
     */
    public List<Session> getReturn() {
        if (_return == null) {
            _return = new ArrayList<Session>();
        }
        return this._return;
    }

}
