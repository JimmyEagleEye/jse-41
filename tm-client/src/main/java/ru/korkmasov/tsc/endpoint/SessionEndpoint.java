package ru.korkmasov.tsc.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://endpoint.tsc.korkmasov.ru/", name = "SessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface SessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/SessionEndpoint/openRequest", output = "http://endpoint.tsc.korkmasov.ru/SessionEndpoint/openResponse")
    @RequestWrapper(localName = "open", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.Open")
    @ResponseWrapper(localName = "openResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.OpenResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Session open(

            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/SessionEndpoint/registerRequest", output = "http://endpoint.tsc.korkmasov.ru/SessionEndpoint/registerResponse")
    @RequestWrapper(localName = "register", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.Register")
    @ResponseWrapper(localName = "registerResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RegisterResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Session register(

            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password,
            @WebParam(name = "email", targetNamespace = "")
                    java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/SessionEndpoint/closeRequest", output = "http://endpoint.tsc.korkmasov.ru/SessionEndpoint/closeResponse")
    @RequestWrapper(localName = "close", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.Close")
    @ResponseWrapper(localName = "closeResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.CloseResponse")
    public void close(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/SessionEndpoint/setPasswordRequest", output = "http://endpoint.tsc.korkmasov.ru/SessionEndpoint/setPasswordResponse")
    @RequestWrapper(localName = "setPassword", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SetPassword")
    @ResponseWrapper(localName = "setPasswordResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SetPasswordResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.UserEndpoint setPassword(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/SessionEndpoint/updateUserRequest", output = "http://endpoint.tsc.korkmasov.ru/SessionEndpoint/updateUserResponse")
    @RequestWrapper(localName = "updateUser", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UpdateUser")
    @ResponseWrapper(localName = "updateUserResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UpdateUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.UserEndpoint updateUser(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "firstName", targetNamespace = "")
                    java.lang.String firstName,
            @WebParam(name = "lastName", targetNamespace = "")
                    java.lang.String lastName,
            @WebParam(name = "middleName", targetNamespace = "")
                    java.lang.String middleName
    );
}
